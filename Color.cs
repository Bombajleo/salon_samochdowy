﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudiCarShowRoom
{
    public class Color
    {
        public int Id;
        public string Name;

        public Color()
        {

        }
        public Color(int Id, string name)
        {
            this.Id = Id;
            this.Name = name;


        }
    }
}
