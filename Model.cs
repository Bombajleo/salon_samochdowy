﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudiCarShowRoom
{
   public class Model
    {
        public string Name;
        public int Id;

        public Model(string name, int id)
        {
            this.Name = name;
            this.Id = id;
        }
    }
}
